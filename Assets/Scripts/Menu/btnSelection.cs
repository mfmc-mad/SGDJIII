using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class btnSelection : MonoBehaviour
{

    /// <summary>
    /// MMC.
    /// Recorro los botones para ver cu�l est� seleccionado. Si corresponde, aplico la animaci�n y el sonido.
    /// </summary>

    #region Variables

    // Variables para recorrer los botones 

    public int iMaxBtnMain;
    public int iMaxBtnLoader;    
    private int iMaxBtnPanel;    
    public Button[] btnMainMenu;
    public Button[] btnLoader;
    private int i;
    private int iDummy = -1; // Le doy un valor de -1 porque as� no coincidir� nunca en la primera pasada con el rango de botones
                             // de los paneles

    // Variables de animaci�n y sonido
    private Animator btnAnimator;
    public AudioClip sndSelected;
    private AudioSource myAudioSource;
    private bool bAlreadyPlayed;
    static bool bAmIinMain;

    // Variables para saber en qu� men� estoy.
    public GameObject goMenuPpal;
    public GameObject goMenuCarga;

    // Variables para seleccionar el primer elemento si cambio de paneles
    public GameObject goBtnFirstMain;
    public GameObject goBtnFirstLoad;
    

    #endregion Variables

    #region Funciones de Unity
    
    private void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
        bAmIinMain = true;
    }

    private void Update()
    {
        WhereAmI();
        btnSelected();
    }

    #endregion Funciones de Unity

    #region Funciones propias

    // Determina en que panel estoy (Main o Loader) y resetea el dummy.
    private void WhereAmI()
    {

        if (goMenuPpal.activeInHierarchy==true)
        {
            // Si vengo del panel de Carga, reseteo el valor de iDummy para recorrer la tabla bien. Tambi�n selecciono el bot�n de 
            // btnStart para no perder el foco.
            if (!bAmIinMain)
            {
                iDummy = -1;
                //EventSystem.current.SetSelectedGameObject(goBtnFirstMain);
            }
            bAmIinMain = true;
        }
        else if(goMenuCarga.activeInHierarchy==true)
        {

            // Si vengo del panel principal, reseteo el valor de iDummy para recorrer la tabla bien.Tambi�n selecciono el bot�n de 
            // btnAtras para no perder el foco.
            if (bAmIinMain)
            {
                iDummy = -1;                
            }
            bAmIinMain = false;
        }
    }

    // Dependiendo de si estoy en el men� principal o en el de cargado, recorro todos los botones que hay y si me muevo de uno a otro
    // activo una animaci�n y un sonido
    private void btnSelected()
    {
        if (bAmIinMain)
        {
            iMaxBtnPanel = iMaxBtnMain;
        }
        else
        {
            iMaxBtnPanel = iMaxBtnLoader;
        }

        for (i = 0; i <= iMaxBtnPanel; i++)
        {
            btnAnimator = btnMainMenu[i].GetComponent<Animator>();
            if (bAmIinMain)
            {
                if (btnMainMenu[i].gameObject == EventSystem.current.currentSelectedGameObject)
                {
                    if (iDummy != i && iDummy != -1)
                    {
                        bAlreadyPlayed = false;
                        btnAnimator.SetBool("Selected", true);
                        PlaySound();
                    }
                    iDummy = i;

                }
                else
                {
                    btnAnimator.SetBool("Selected", false);
                }
            }
            else
            {
                if (btnLoader[i].gameObject == EventSystem.current.currentSelectedGameObject)
                {
                    if (iDummy != i && iDummy != -1)
                    {
                        bAlreadyPlayed = false;
                        btnAnimator.SetBool("Selected", true);
                        PlaySound();
                    }
                    iDummy = i;

                }
                else
                {
                    btnAnimator.SetBool("Selected", false);
                }
            }

        }
    }

    private void PlaySound()
    {
        if (!bAlreadyPlayed)
        {
            myAudioSource.PlayOneShot(sndSelected);
            bAlreadyPlayed = true;
        }
    }



    #endregion Funciones propias    
}
