using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLoad : MonoBehaviour
{
    /// <summary>
    /// MMC
    /// Carga la primera escena o sale del juego (New Game o Exit).
    /// </summary>
    /// 

    #region Variables

    public Animator fadeAnima;
    public Animator btnAnima;
    public AudioSource btnPressed;

    #endregion Variables


    #region Funciones propias
    public void StartGame()
    {
        StartCoroutine(FadeAndLoadNewGame());
    }

    public void ExitGame()
    {
        Application.Quit();
        Debug.Log("Salgo");
    }

    IEnumerator FadeAndLoadNewGame()
    {
        btnAnima.SetBool("Pressed", true);
        btnPressed.Play(0);
        yield return new WaitForSeconds(1f);
        fadeAnima.SetBool("FadeOut", true);
        yield return new WaitForSeconds(2f);
        // La primera escena debe estar Indexada inmediatamente despu�s del men�.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    #endregion Funciones propias
}
