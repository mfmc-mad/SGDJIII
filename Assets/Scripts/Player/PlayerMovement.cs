﻿using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// MMC
/// Script que se encarga del movimiento del player 
/// </summary>


public class PlayerMovement : MonoBehaviour
{

    #region Variables
    
    // Variables de velocidad en x y fuerza en salto (y)
    [SerializeField] private float playerSpeed = 4;
    [SerializeField] private float jumpForce = 10;

    // Variables relacionadas con el movimiento
    private float xDir = 0f;
    private float yDir = 0f;
    private bool bRejump = false;

    // Rigid body para las físicas
    private Rigidbody2D myColdDeadRigidBody;

    // Variables para determinar si estoy sobre el suelo
    public Transform refFoot;
    private float circleRadius=0.1f;

    #endregion Variables

    #region Funciones de Unity


    private void Start()
    {
        myColdDeadRigidBody = GetComponent<Rigidbody2D>();     
    }


    private void FixedUpdate()
    {
        AddMovement();
    }

    #endregion Funciones propias de Unity

    #region PlayerInput Events

    ///<summary>
    /// Estas funciones se configuran en componente PlayerInput del player, son eventos asociables en el Inspector (PlayerInput->Events).
    /// Reciben la entrada recibida por el binding creado en la Action (Jump, Movement, etc) en InputActions y la guarda en la 
    /// variable _context
    ///</summary>
 
    // Función de salto con input recogido mediante InputSystem
    public void Jump(InputAction.CallbackContext _context)
    {
        // Para que la fuerza que se añade en esta función no se ejecute dos veces (started y performed), incluímos una condicional
        // que haga que aplique la fuerza sólo en el estado performed.

        if (_context.performed)
        {

            if (CheckOnGround())
            {
                myColdDeadRigidBody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                bRejump = true;
            }
            else
            {
                if (bRejump)
                {
                myColdDeadRigidBody.AddForce(Vector2.up * jumpForce/2, ForceMode2D.Impulse);
                bRejump = false;
                }
            }
        }
    }

    // Función de movimiento con input recogido mediante InputSystem
    public void Movement(InputAction.CallbackContext _context)
    {
        if (_context.performed)
        {            
            xDir = _context.ReadValue<Vector2>().x;
            yDir = _context.ReadValue<Vector2>().y;
        }
        // Si no hay input, paro.
        if (_context.canceled)
        {            
            xDir = 0;
            yDir = 0;
        }
    }

    #endregion PlayerInput Events

    #region Funciones Propias
    // Comprobación de si tocamos suelo para poder saltar
    private bool CheckOnGround()
    {
        // Si, desde el punto que tomamos como referencia del pie, en un radio de 0,1 está la capa número 8 (Platform), 
        // estoy en el suelo y por lo tanto puedo saltar.

        if (Physics2D.OverlapCircle(refFoot.position, circleRadius, 1 << 8))
        {
            return (true);
        }
        else
        {
            return(false);
        }
    }
    private void AddMovement()
    {
        myColdDeadRigidBody.velocity = new Vector2((playerSpeed * xDir), myColdDeadRigidBody.velocity.y);
    }


    // En el entorno de FixedUpdate, aplicamos la fuerza con los valores recogidos en OnMovement

    #endregion funciones Propias

}